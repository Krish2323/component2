﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Software_Assignment
{
    class circle :Shape
    {

        //declare variables
        int  radious;


        /// Parameter constructor        
        public circle(int X, int Y, int Rad)
        {
            this.X = X;
            this.Y = Y;
            this.radious = Rad;
        }

        /// another parameter constructor        
        public circle(int x, int y) : base(x, y)
        {

        }

        /// default constructor       
        public circle()
        {


        }



        /// draw method

        public override void drawObj(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c, thickness);
            g.DrawEllipse(p, X, Y, radious, radious);
        }


      
        public void setradious(int rad)
        {
            this.radious = rad;
        }

        public int getradious()
        {
            return this.radious;
        }

    }




}
