﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Software_Assignment;

namespace Software_Assignment
{
    abstract class Shape : shapeinterface
    {
            protected Color col;
            protected int X = 0, Y = 0;
            public Shape()
            {
                col = Color.Red;
            }


            public Shape(int x, int y)
            {
                this.X = x;
                this.Y = y;

            }


            public abstract void drawObj(Graphics g, Color c, int thick);




            public virtual void setcolor(Color colour)
            {
                this.col = colour;

            }
        public void setx(int x)
        {
            this.X = x;
        }
        public void setY(int y)
        {
            this.Y = y;
        }

        public int getx()
        {
            return X;
        }
        public int getY()
        {
            return Y;
        }



        public override string ToString()
            {
                return base.ToString() + "    " + this.X + "," + this.Y + " : ";
            }


       
    
    }

    }
