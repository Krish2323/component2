﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Software_Assignment
{
    interface shapeinterface
    {
        void setcolor(Color cl);

        void setx(int x);
        int getx();

        void setY(int y);
        int getY();
        void drawObj(Graphics g, Color c, int thick);
    }
}
