﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Software_Assignment
{
    class square:Shape
    {
        //declare variables
        int x, y, length;


        /// Parameter constructor        
        public square(int X, int Y, int lenth)
        {
            this.x = X;
            this.y = Y;
            this.length = lenth;
        }

        /// another parameter constructor        
        public square(int x, int y) : base(x, y)
        {

        }

        /// default constructor       
        public square()
        {


        }



        public override void drawObj(Graphics g, Color c, int thick)
        {
            Pen p = new Pen(c, thick);
            g.DrawRectangle(p, x, y, length, length);
        }


       
        public void setleng(int length)
        {
            this.length = length;
        }

        public int getleng()
        {
            return this.length;
        }
        


    }
}
