﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Software_Assignment
{
    class rectangle : Shape
    {
        //declare variables
        int height, width;


        /// Parameter constructor        
        public rectangle(int x, int y, int height, int width) : base(x, y)
        {
            this.height = height;
            this.width = width;
        }

        /// another parameter constructor        
        public rectangle(int x, int y) : base(x, y)
        {

        }

        /// default constructor       
        public rectangle()
        {
        }



        /// draw method

        public override void drawObj(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c, thickness);
            g.DrawRectangle(p, X, Y, height, width);
        }


        /// height setter

        public void setHeight(int height)
        {
            this.height = height;
        }



        /// height getter

        public int getHeight()
        {
            return this.height;
        }

        public void setWidth(int width)
        {
            this.width = width;
        }



        public int getWidth()
        {
            return this.width;
        }




    }
}
