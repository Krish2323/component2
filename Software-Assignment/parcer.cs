﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Software_Assignment
{
    class parcer
    {
        int moveX, moveY;

        List<string> loopContent;



        Color c = Color.Black;
        Color fill = Color.Transparent;
    

        String Error;
        Boolean error;
        Graphics g;

        Bitmap bitmapvalue = new Bitmap(799, 999);
        public String parcerclass(string command, string code)
        {
            string comm = command.ToLower();

            switch (command)
            {
                case "run":
                    try
                    {
                        char[] delimiters = new char[] { '\r', '\n' };
                        string[] parts = code.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        loopContent = new List<string>();
                        for (int i = 0; i < parts.Length; i++)
                        {

                            if (this.loop == true)
                            {
                                loopContent.Add(parts[i]);
                            }
                            else
                            {

                                check(parts[i], i + 1);
                            }
                        }

                        for (int k = 0; k < loopSize; k++)
                        {
                            for (int j = 0; j < loopContent.Count - 1; j++)
                            {

                                check(loopContent[j], k);
                            }

                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {

                        return ex.Message;

                    }
                    catch (FormatException ex)
                    {
                        MessageBox.Show("");
                        return "!!Please input correct parameter!!";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    break;
                case "clear":

                    break;
                case "reset":
                    this.moveX = 0;
                    this.moveY = 0;
                    this.thickness = 2;
                    this.fill = Color.Transparent;
                    c = Color.Black;
                    return "Pen reset to default value";
                case "help":
                    return "For Your Help:\n" +
                             "draw circle 100\n" +
                             "draw rectangle 100 50\n" +
                             "draw triangle 10 10 100 10 50 60\n" +
                             "draw square 50\n" +
                             "move 100 100\n" +
                             "color red 23\n" +
                             "fill red\n" +
                             "fill no\n";
                default:
                    MessageBox.Show("");
                    return "Please enter a command";

            }

            return null;


        }

        public parcer()
        {
            bitmapvalue = new Bitmap(900, 700);
            g = Graphics.FromImage(bitmapvalue);
        }


        public Bitmap getBitmap()
        {
            return this.bitmapvalue;

        }
        int height, width, radious, squaresize;


        public string check(string code, int line)
        {
            int temp;

            char[] code_delimiters = new char[] { ' ' };
            String[] words = code.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);

            if (words[0] == "draw")
            {
                if (words[1] == "circle")
                {
                    if (!(words.Length == 3))
                    {

                        MessageBox.Show("");
                        error = true;
                        Error = "The error in on line " + line + " please type correct code for 'draw circle'";
                        return Error;
                    }
                    else if (Int32.TryParse(words[2], out temp))
                    {
                        circle circle = new circle();
                        circle.setcolor(fill);
                        circle.setx(moveX);
                        circle.setY(moveY);
                        circle.setradious(Convert.ToInt32(words[2]));
                        circle.drawObj(g, c, thickness);
                        //return null;
                    }
                    else if (words[2] == "radious")
                    {
                        circle circle = new circle();
                        circle.setcolor(fill);
                        circle.setx(moveX);
                        circle.setY(moveY);
                        circle.setradious(this.radious);
                        circle.drawObj(g, c, thickness);

                    }

                    else
                    {
                        MessageBox.Show("");
                        return "Please enter The parameters in integers in line " + line;

                    }
                }

                else if (words[1] == "square")
                {
                    if (!(words.Length == 3))
                    {
                        error = true;
                        Error = "The error in on line " + line + " Please type correct code for 'draw square'";
                        return Error;
                    }
                    else if (Int32.TryParse(words[2], out temp))
                    {
                        square square = new square();
                        square.setcolor(fill);
                        square.setx(moveX);
                        square.setY(moveY);
                        square.setleng(Convert.ToInt32(words[2]));
                        square.drawObj(g, c, thickness);
                        //return null;
                    }
                    else if (words[2] == "squarelngth")
                    {
                        square square = new square();
                        square.setcolor(fill);
                        square.setx(moveX);
                        square.setY(moveY);
                        square.setleng(this.squaresize);
                        square.drawObj(g, c, thickness);

                    }
                    else
                    {
                        MessageBox.Show("");
                        return "Please enter The parameters in integers in line " + line;

                    }
                }

                else if (words[1] == "rectangle")
                {
                    if (!(words.Length == 4))
                    {
                        MessageBox.Show("");
                        error = true;
                        Error = "The error in on line " + line + " Please type correct code for 'draw rectangle'";

                    }
                    else if (Int32.TryParse(words[2], out temp) && Int32.TryParse(words[3], out temp))
                    {
                        rectangle rectangle = new rectangle();
                        rectangle.setcolor(fill);
                        rectangle.setx(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(Convert.ToInt32(words[2]));
                        rectangle.setWidth(Convert.ToInt32(words[3]));
                        rectangle.drawObj(g, c, thickness);
                        //return null;
                    }
                    else if ((words[2] == "height" || words[2] == "width") && (words[3] == "height" || words[3] == "width"))
                    {
                        rectangle rectangle = new rectangle();
                        rectangle.setcolor(fill);
                        rectangle.setx(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(height);
                        rectangle.setWidth(width);
                        rectangle.drawObj(g, c, thickness);
                    }
                    else
                    {
                        MessageBox.Show("");
                        return "Please enter The parameters in integers in line " + line;

                    }
                }
                else if (words[1] == "triangle")
                {
                    if (!(words.Length == 8))
                    {
                        error = true;
                        Error = "The error in on line " + line + " Please type correct code for 'draw triangle'";

                    }
                    else if (Int32.TryParse(words[2], out temp) && Int32.TryParse(words[3], out temp) && Int32.TryParse(words[4], out temp) && Int32.TryParse(words[5], out temp) && Int32.TryParse(words[6], out temp) && Int32.TryParse(words[7], out temp))
                    {
                        triangle triangle = new triangle();
                        triangle.setcolor(fill);
                        triangle.drawObj(g, c, thickness);
                        //return null;
                    }
                    else
                    {
                        MessageBox.Show("");
                        return "Please enter The parameters in integers in line " + line;

                    }
                }
                else
                {
                    MessageBox.Show("");
                    return "Please enter correct code in line " + line;
                }
            }
            else if (words[0] == "radious")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.radious = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.radious = radious + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "height")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.height = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.height = height + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "width")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.width = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.width = width + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "squaresize")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.squaresize = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.squaresize = squaresize + Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("");
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "move")
            {
                if (!(words.Length == 3))
                {
                    error = true;
                    Error = "The error in on line " + line + " Please type correct code for 'move'";
                    return Error;
                }
                else if (Int32.TryParse(words[1], out temp) && Int32.TryParse(words[2], out temp))
                {
                    this.moveX = Convert.ToInt32(words[1]);
                    this.moveY = Convert.ToInt32(words[2]);
                }
                else
                {
                    MessageBox.Show("");
                    return "Please enter The parameters in integers in line " + line;

                }
            }
            else if (words[0] == "color")
            {
                if (!(words.Length == 3))
                {
                    error = true;
                    Error = "The error in on line " + line + " Please type correct code for 'move'";
                    return Error;
                }
                else if (Int32.TryParse(words[2], out temp))
                {
                    this.thickness = Convert.ToInt32(words[2]);
                    Color cs = Color.FromName(words[1]);

                    if (cs.IsKnownColor)
                    {
                        this.c = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));
                        //return "color of pen changed to " + char.ToUpper(words[1][0]) + words[1].Substring(1);
                    }
                    else
                    {
                        MessageBox.Show("");
                        return ("please enter a valid color");
                    }
                }
                else
                {
                    MessageBox.Show("");
                    return "Please enter The parameters in integers" + line;

                }
            }
            else if (words[0] == "fill")
            {
                if (!(words.Length == 2))
                {
                    error = true;
                    Error = "The error in on line " + line + " Please type correct code for 'move'";
                    return Error;
                }

                else
                {
                    Color fil = Color.FromName(words[1]);
                    if (fil.IsKnownColor)
                    {
                        this.fill = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));
                    }
                    else if (words[1] == "no")
                    {
                        this.fill = Color.Transparent;

                    }
                    else
                    {
                        MessageBox.Show("");
                        return ("please enter a valid color");
                    }
                }
            }
            else if (words[0] == "loop")
            {
                this.loop = true;
                this.loopSize = Convert.ToInt32(words[1]);
            }
            else if (words[0] == "endloop")
            {
                loop = false;
                loopSize = 0;
            }
            else
            {
                MessageBox.Show("");
                return "Please enter The correct codes";
            }

            return null;
        }
        int thickness = 3;


        int loopSize;
        Boolean loop = false;


    }
}
