﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Software_Assignment
{
    class triangle : Shape
    {
        //declare variables
        int height, width;


        /// Parameter constructor        
        public triangle(int x, int y, int height, int width) : base(x, y)
        {
            this.height = height;
            this.width = width;
        }

        /// another parameter constructor        
        public triangle(int x, int y) : base(x, y)
        {

        }

        /// default constructor       
        public triangle()
        {
        }



        /// draw method

        public override void drawObj(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c, thickness);
            g.DrawRectangle(p, X, Y, height, width);
        }


      

        public void setheight(int height)
        {
            this.height = height;
        }



     

        public int getheight()
        {
            return this.height;
        }

        public void setwidth(int width)
        {
            this.width = width;
        }



        public int getwidth()
        {
            return this.width;
        }




    }
}
